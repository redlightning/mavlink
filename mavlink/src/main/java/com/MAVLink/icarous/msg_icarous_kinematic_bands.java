package com.MAVLink.icarous;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_icarous_kinematic_bands extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_ICAROUS_KINEMATIC_BANDS = 42001;
    public static final int MAVLINK_MSG_LENGTH = 46;
    private static final long serialVersionUID = 42001;
    public float max1;
    public float max2;
    public float max3;
    public float max4;
    public float max5;
    public float min1;
    public float min2;
    public float min3;
    public float min4;
    public float min5;
    public byte numBands;
    public short type1;
    public short type2;
    public short type3;
    public short type4;
    public short type5;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(46, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = 42001;
        mAVLinkPacket.payload.putFloat(this.min1);
        mAVLinkPacket.payload.putFloat(this.max1);
        mAVLinkPacket.payload.putFloat(this.min2);
        mAVLinkPacket.payload.putFloat(this.max2);
        mAVLinkPacket.payload.putFloat(this.min3);
        mAVLinkPacket.payload.putFloat(this.max3);
        mAVLinkPacket.payload.putFloat(this.min4);
        mAVLinkPacket.payload.putFloat(this.max4);
        mAVLinkPacket.payload.putFloat(this.min5);
        mAVLinkPacket.payload.putFloat(this.max5);
        mAVLinkPacket.payload.putByte(this.numBands);
        mAVLinkPacket.payload.putUnsignedByte(this.type1);
        mAVLinkPacket.payload.putUnsignedByte(this.type2);
        mAVLinkPacket.payload.putUnsignedByte(this.type3);
        mAVLinkPacket.payload.putUnsignedByte(this.type4);
        mAVLinkPacket.payload.putUnsignedByte(this.type5);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.min1 = mAVLinkPayload.getFloat();
        this.max1 = mAVLinkPayload.getFloat();
        this.min2 = mAVLinkPayload.getFloat();
        this.max2 = mAVLinkPayload.getFloat();
        this.min3 = mAVLinkPayload.getFloat();
        this.max3 = mAVLinkPayload.getFloat();
        this.min4 = mAVLinkPayload.getFloat();
        this.max4 = mAVLinkPayload.getFloat();
        this.min5 = mAVLinkPayload.getFloat();
        this.max5 = mAVLinkPayload.getFloat();
        this.numBands = mAVLinkPayload.getByte();
        this.type1 = mAVLinkPayload.getUnsignedByte();
        this.type2 = mAVLinkPayload.getUnsignedByte();
        this.type3 = mAVLinkPayload.getUnsignedByte();
        this.type4 = mAVLinkPayload.getUnsignedByte();
        this.type5 = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_icarous_kinematic_bands() {
        this.msgid = 42001;
    }

    public msg_icarous_kinematic_bands(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = 42001;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_ICAROUS_KINEMATIC_BANDS - sysid:" + this.sysid + " compid:" + this.compid + " min1:" + this.min1 + " max1:" + this.max1 + " min2:" + this.min2 + " max2:" + this.max2 + " min3:" + this.min3 + " max3:" + this.max3 + " min4:" + this.min4 + " max4:" + this.max4 + " min5:" + this.min5 + " max5:" + this.max5 + " numBands:" + this.numBands + " type1:" + this.type1 + " type2:" + this.type2 + " type3:" + this.type3 + " type4:" + this.type4 + " type5:" + this.type5 + "";
    }
}
