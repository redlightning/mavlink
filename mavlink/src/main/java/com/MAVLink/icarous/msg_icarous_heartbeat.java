package com.MAVLink.icarous;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_icarous_heartbeat extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_ICAROUS_HEARTBEAT = 42000;
    public static final int MAVLINK_MSG_LENGTH = 1;
    private static final long serialVersionUID = 42000;
    public short status;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(1, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = 42000;
        mAVLinkPacket.payload.putUnsignedByte(this.status);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.status = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_icarous_heartbeat() {
        this.msgid = 42000;
    }

    public msg_icarous_heartbeat(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = 42000;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_ICAROUS_HEARTBEAT - sysid:" + this.sysid + " compid:" + this.compid + " status:" + this.status + "";
    }
}
