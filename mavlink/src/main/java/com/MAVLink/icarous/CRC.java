package com.MAVLink.icarous;

//TODO import com.google.vr.cardboard.StoragePermissionUtils;
import java.util.HashMap;
import java.util.Map;

public class CRC {
    private static final int CRC_INIT_VALUE = 65535;
    private static final Map<Integer, Integer> MAVLINK_MESSAGE_CRCS = new HashMap();
    private int crcValue;

    static {
        MAVLINK_MESSAGE_CRCS.put(42000, 227);
        //TODO MAVLINK_MESSAGE_CRCS.put(42001, Integer.valueOf(StoragePermissionUtils.STORAGE_PERMISSION_DUMMY_REQUEST_CODE));
    }

    public void update_checksum(int i) {
        int i2 = (i & 255) ^ (this.crcValue & 255);
        int i3 = i2 ^ ((i2 << 4) & 255);
        this.crcValue = ((i3 >> 4) & 15) ^ ((((this.crcValue >> 8) & 255) ^ (i3 << 8)) ^ (i3 << 3));
    }

    public void finish_checksum(int i) {
        if (MAVLINK_MESSAGE_CRCS.containsKey(i)) {
            update_checksum(MAVLINK_MESSAGE_CRCS.get(i));
        }
    }

    public void start_checksum() {
        this.crcValue = 65535;
    }

    public int getMSB() {
        return (this.crcValue >> 8) & 255;
    }

    public int getLSB() {
        return this.crcValue & 255;
    }

    public CRC() {
        start_checksum();
    }
}
