package com.MAVLink.uAvionix;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_uavionix_adsb_out_dynamic extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_UAVIONIX_ADSB_OUT_DYNAMIC = 10002;
    public static final int MAVLINK_MSG_LENGTH = 41;
    private static final long serialVersionUID = 10002;
    public short VelEW;
    public long accuracyHor;
    public int accuracyVel;
    public int accuracyVert;
    public int baroAltMSL;
    public short emergencyStatus;
    public int gpsAlt;
    public short gpsFix;
    public int gpsLat;
    public int gpsLon;
    public short numSats;
    public int squawk;
    public int state;
    public long utcTime;
    public short velNS;
    public short velVert;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(41, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = 10002;
        mAVLinkPacket.payload.putUnsignedInt(this.utcTime);
        mAVLinkPacket.payload.putInt(this.gpsLat);
        mAVLinkPacket.payload.putInt(this.gpsLon);
        mAVLinkPacket.payload.putInt(this.gpsAlt);
        mAVLinkPacket.payload.putInt(this.baroAltMSL);
        mAVLinkPacket.payload.putUnsignedInt(this.accuracyHor);
        mAVLinkPacket.payload.putUnsignedShort(this.accuracyVert);
        mAVLinkPacket.payload.putUnsignedShort(this.accuracyVel);
        mAVLinkPacket.payload.putShort(this.velVert);
        mAVLinkPacket.payload.putShort(this.velNS);
        mAVLinkPacket.payload.putShort(this.VelEW);
        mAVLinkPacket.payload.putUnsignedShort(this.state);
        mAVLinkPacket.payload.putUnsignedShort(this.squawk);
        mAVLinkPacket.payload.putUnsignedByte(this.gpsFix);
        mAVLinkPacket.payload.putUnsignedByte(this.numSats);
        mAVLinkPacket.payload.putUnsignedByte(this.emergencyStatus);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.utcTime = mAVLinkPayload.getUnsignedInt();
        this.gpsLat = mAVLinkPayload.getInt();
        this.gpsLon = mAVLinkPayload.getInt();
        this.gpsAlt = mAVLinkPayload.getInt();
        this.baroAltMSL = mAVLinkPayload.getInt();
        this.accuracyHor = mAVLinkPayload.getUnsignedInt();
        this.accuracyVert = mAVLinkPayload.getUnsignedShort();
        this.accuracyVel = mAVLinkPayload.getUnsignedShort();
        this.velVert = mAVLinkPayload.getShort();
        this.velNS = mAVLinkPayload.getShort();
        this.VelEW = mAVLinkPayload.getShort();
        this.state = mAVLinkPayload.getUnsignedShort();
        this.squawk = mAVLinkPayload.getUnsignedShort();
        this.gpsFix = mAVLinkPayload.getUnsignedByte();
        this.numSats = mAVLinkPayload.getUnsignedByte();
        this.emergencyStatus = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_uavionix_adsb_out_dynamic() {
        this.msgid = 10002;
    }

    public msg_uavionix_adsb_out_dynamic(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = 10002;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_UAVIONIX_ADSB_OUT_DYNAMIC - sysid:" + this.sysid + " compid:" + this.compid + " utcTime:" + this.utcTime + " gpsLat:" + this.gpsLat + " gpsLon:" + this.gpsLon + " gpsAlt:" + this.gpsAlt + " baroAltMSL:" + this.baroAltMSL + " accuracyHor:" + this.accuracyHor + " accuracyVert:" + this.accuracyVert + " accuracyVel:" + this.accuracyVel + " velVert:" + this.velVert + " velNS:" + this.velNS + " VelEW:" + this.VelEW + " state:" + this.state + " squawk:" + this.squawk + " gpsFix:" + this.gpsFix + " numSats:" + this.numSats + " emergencyStatus:" + this.emergencyStatus + "";
    }
}
