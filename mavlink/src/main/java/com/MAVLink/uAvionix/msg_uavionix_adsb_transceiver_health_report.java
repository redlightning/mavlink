package com.MAVLink.uAvionix;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_uavionix_adsb_transceiver_health_report extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_UAVIONIX_ADSB_TRANSCEIVER_HEALTH_REPORT = 10003;
    public static final int MAVLINK_MSG_LENGTH = 1;
    private static final long serialVersionUID = 10003;
    public short rfHealth;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(1, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = 10003;
        mAVLinkPacket.payload.putUnsignedByte(this.rfHealth);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.rfHealth = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_uavionix_adsb_transceiver_health_report() {
        this.msgid = 10003;
    }

    public msg_uavionix_adsb_transceiver_health_report(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = 10003;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_UAVIONIX_ADSB_TRANSCEIVER_HEALTH_REPORT - sysid:" + this.sysid + " compid:" + this.compid + " rfHealth:" + this.rfHealth + "";
    }
}
