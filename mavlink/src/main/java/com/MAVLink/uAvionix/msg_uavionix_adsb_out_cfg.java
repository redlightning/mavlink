package com.MAVLink.uAvionix;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_uavionix_adsb_out_cfg extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_UAVIONIX_ADSB_OUT_CFG = 10001;
    public static final int MAVLINK_MSG_LENGTH = 20;
    private static final long serialVersionUID = 10001;
    public long ICAO;
    public short aircraftSize;
    public byte[] callsign = new byte[9];
    public short emitterType;
    public short gpsOffsetLat;
    public short gpsOffsetLon;
    public short rfSelect;
    public int stallSpeed;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(20, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = 10001;
        mAVLinkPacket.payload.putUnsignedInt(this.ICAO);
        mAVLinkPacket.payload.putUnsignedShort(this.stallSpeed);
        for (byte putByte : this.callsign) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        mAVLinkPacket.payload.putUnsignedByte(this.emitterType);
        mAVLinkPacket.payload.putUnsignedByte(this.aircraftSize);
        mAVLinkPacket.payload.putUnsignedByte(this.gpsOffsetLat);
        mAVLinkPacket.payload.putUnsignedByte(this.gpsOffsetLon);
        mAVLinkPacket.payload.putUnsignedByte(this.rfSelect);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.ICAO = mAVLinkPayload.getUnsignedInt();
        this.stallSpeed = mAVLinkPayload.getUnsignedShort();
        for (int i = 0; i < this.callsign.length; i++) {
            this.callsign[i] = mAVLinkPayload.getByte();
        }
        this.emitterType = mAVLinkPayload.getUnsignedByte();
        this.aircraftSize = mAVLinkPayload.getUnsignedByte();
        this.gpsOffsetLat = mAVLinkPayload.getUnsignedByte();
        this.gpsOffsetLon = mAVLinkPayload.getUnsignedByte();
        this.rfSelect = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_uavionix_adsb_out_cfg() {
        this.msgid = 10001;
    }

    public msg_uavionix_adsb_out_cfg(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = 10001;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setCallsign(String str) {
        int min = Math.min(str.length(), 9);
        for (int i = 0; i < min; i++) {
            this.callsign[i] = (byte) str.charAt(i);
        }
        while (min < 9) {
            this.callsign[min] = 0;
            min++;
        }
    }

    public String getCallsign() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 9 && this.callsign[i] != 0) {
            stringBuffer.append((char) this.callsign[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_UAVIONIX_ADSB_OUT_CFG - sysid:" + this.sysid + " compid:" + this.compid + " ICAO:" + this.ICAO + " stallSpeed:" + this.stallSpeed + " callsign:" + this.callsign + " emitterType:" + this.emitterType + " aircraftSize:" + this.aircraftSize + " gpsOffsetLat:" + this.gpsOffsetLat + " gpsOffsetLon:" + this.gpsOffsetLon + " rfSelect:" + this.rfSelect + "";
    }
}
