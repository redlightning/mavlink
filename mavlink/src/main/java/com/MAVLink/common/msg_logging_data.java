package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_logging_data extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_LOGGING_DATA = 266;
    public static final int MAVLINK_MSG_LENGTH = 255;
    private static final long serialVersionUID = 266;
    public short[] data = new short[msg_memory_vect.MAVLINK_MSG_ID_MEMORY_VECT];
    public short first_message_offset;
    public short length;
    public int sequence;
    public short target_component;
    public short target_system;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(255, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_LOGGING_DATA;
        mAVLinkPacket.payload.putUnsignedShort(this.sequence);
        mAVLinkPacket.payload.putUnsignedByte(this.target_system);
        mAVLinkPacket.payload.putUnsignedByte(this.target_component);
        mAVLinkPacket.payload.putUnsignedByte(this.length);
        mAVLinkPacket.payload.putUnsignedByte(this.first_message_offset);
        for (short putUnsignedByte : this.data) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.sequence = mAVLinkPayload.getUnsignedShort();
        this.target_system = mAVLinkPayload.getUnsignedByte();
        this.target_component = mAVLinkPayload.getUnsignedByte();
        this.length = mAVLinkPayload.getUnsignedByte();
        this.first_message_offset = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = mAVLinkPayload.getUnsignedByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_logging_data() {
        this.msgid = MAVLINK_MSG_ID_LOGGING_DATA;
    }

    public msg_logging_data(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_LOGGING_DATA;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_LOGGING_DATA - sysid:" + this.sysid + " compid:" + this.compid + " sequence:" + this.sequence + " target_system:" + this.target_system + " target_component:" + this.target_component + " length:" + this.length + " first_message_offset:" + this.first_message_offset + " data:" + this.data + "";
    }
}
