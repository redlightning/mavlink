package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_play_tune extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_PLAY_TUNE = 258;
    public static final int MAVLINK_MSG_LENGTH = 232;
    private static final long serialVersionUID = 258;
    public short target_component;
    public short target_system;
    public byte[] tune = new byte[30];
    public byte[] tune2 = new byte[200];

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(232, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_PLAY_TUNE;
        mAVLinkPacket.payload.putUnsignedByte(this.target_system);
        mAVLinkPacket.payload.putUnsignedByte(this.target_component);
        for (byte putByte : this.tune) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        if (this.isMavlink2) {
            for (byte putByte2 : this.tune2) {
                mAVLinkPacket.payload.putByte(putByte2);
            }
        }
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.target_system = mAVLinkPayload.getUnsignedByte();
        this.target_component = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.tune.length; i++) {
            this.tune[i] = mAVLinkPayload.getByte();
        }
        if (this.isMavlink2) {
            for (int i2 = 0; i2 < this.tune2.length; i2++) {
                this.tune2[i2] = mAVLinkPayload.getByte();
            }
        }
    }

    public msg_play_tune() {
        this.msgid = MAVLINK_MSG_ID_PLAY_TUNE;
    }

    public msg_play_tune(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_PLAY_TUNE;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setTune(String str) {
        int min = Math.min(str.length(), 30);
        for (int i = 0; i < min; i++) {
            this.tune[i] = (byte) str.charAt(i);
        }
        while (min < 30) {
            this.tune[min] = 0;
            min++;
        }
    }

    public String getTune() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 30 && this.tune[i] != 0) {
            stringBuffer.append((char) this.tune[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public void setTune2(String str) {
        int min = Math.min(str.length(), 200);
        for (int i = 0; i < min; i++) {
            this.tune2[i] = (byte) str.charAt(i);
        }
        while (min < 200) {
            this.tune2[min] = 0;
            min++;
        }
    }

    public String getTune2() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 200 && this.tune2[i] != 0) {
            stringBuffer.append((char) this.tune2[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_PLAY_TUNE - sysid:" + this.sysid + " compid:" + this.compid + " target_system:" + this.target_system + " target_component:" + this.target_component + " tune:" + this.tune + " tune2:" + this.tune2 + "";
    }
}
