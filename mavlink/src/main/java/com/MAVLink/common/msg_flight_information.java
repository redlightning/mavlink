package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_flight_information extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_FLIGHT_INFORMATION = 264;
    public static final int MAVLINK_MSG_LENGTH = 28;
    private static final long serialVersionUID = 264;
    public long arming_time_utc;
    public long flight_uuid;
    public long takeoff_time_utc;
    public long time_boot_ms;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(28, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_FLIGHT_INFORMATION;
        mAVLinkPacket.payload.putUnsignedLong(this.arming_time_utc);
        mAVLinkPacket.payload.putUnsignedLong(this.takeoff_time_utc);
        mAVLinkPacket.payload.putUnsignedLong(this.flight_uuid);
        mAVLinkPacket.payload.putUnsignedInt(this.time_boot_ms);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.arming_time_utc = mAVLinkPayload.getUnsignedLong();
        this.takeoff_time_utc = mAVLinkPayload.getUnsignedLong();
        this.flight_uuid = mAVLinkPayload.getUnsignedLong();
        this.time_boot_ms = mAVLinkPayload.getUnsignedInt();
        boolean z = this.isMavlink2;
    }

    public msg_flight_information() {
        this.msgid = MAVLINK_MSG_ID_FLIGHT_INFORMATION;
    }

    public msg_flight_information(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_FLIGHT_INFORMATION;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_FLIGHT_INFORMATION - sysid:" + this.sysid + " compid:" + this.compid + " arming_time_utc:" + this.arming_time_utc + " takeoff_time_utc:" + this.takeoff_time_utc + " flight_uuid:" + this.flight_uuid + " time_boot_ms:" + this.time_boot_ms + "";
    }
}
