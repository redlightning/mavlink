package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_camera_image_captured extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED = 263;
    public static final int MAVLINK_MSG_LENGTH = 255;
    private static final long serialVersionUID = 263;
    public int alt;
    public short camera_id;
    public byte capture_result;
    public byte[] file_url = new byte[205];
    public int image_index;
    public int lat;
    public int lon;
    public float[] q = new float[4];
    public int relative_alt;
    public long time_boot_ms;
    public long time_utc;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(255, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED;
        mAVLinkPacket.payload.putUnsignedLong(this.time_utc);
        mAVLinkPacket.payload.putUnsignedInt(this.time_boot_ms);
        mAVLinkPacket.payload.putInt(this.lat);
        mAVLinkPacket.payload.putInt(this.lon);
        mAVLinkPacket.payload.putInt(this.alt);
        mAVLinkPacket.payload.putInt(this.relative_alt);
        for (float putFloat : this.q) {
            mAVLinkPacket.payload.putFloat(putFloat);
        }
        mAVLinkPacket.payload.putInt(this.image_index);
        mAVLinkPacket.payload.putUnsignedByte(this.camera_id);
        mAVLinkPacket.payload.putByte(this.capture_result);
        for (byte putByte : this.file_url) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_utc = mAVLinkPayload.getUnsignedLong();
        this.time_boot_ms = mAVLinkPayload.getUnsignedInt();
        this.lat = mAVLinkPayload.getInt();
        this.lon = mAVLinkPayload.getInt();
        this.alt = mAVLinkPayload.getInt();
        this.relative_alt = mAVLinkPayload.getInt();
        for (int i = 0; i < this.q.length; i++) {
            this.q[i] = mAVLinkPayload.getFloat();
        }
        this.image_index = mAVLinkPayload.getInt();
        this.camera_id = mAVLinkPayload.getUnsignedByte();
        this.capture_result = mAVLinkPayload.getByte();
        for (int i2 = 0; i2 < this.file_url.length; i2++) {
            this.file_url[i2] = mAVLinkPayload.getByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_camera_image_captured() {
        this.msgid = MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED;
    }

    public msg_camera_image_captured(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setFile_Url(String str) {
        int min = Math.min(str.length(), 205);
        for (int i = 0; i < min; i++) {
            this.file_url[i] = (byte) str.charAt(i);
        }
        while (min < 205) {
            this.file_url[min] = 0;
            min++;
        }
    }

    public String getFile_Url() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 205 && this.file_url[i] != 0) {
            stringBuffer.append((char) this.file_url[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_CAMERA_IMAGE_CAPTURED - sysid:" + this.sysid + " compid:" + this.compid + " time_utc:" + this.time_utc + " time_boot_ms:" + this.time_boot_ms + " lat:" + this.lat + " lon:" + this.lon + " alt:" + this.alt + " relative_alt:" + this.relative_alt + " q:" + this.q + " image_index:" + this.image_index + " camera_id:" + this.camera_id + " capture_result:" + this.capture_result + " file_url:" + this.file_url + "";
    }
}
