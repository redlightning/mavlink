package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_button_change extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_BUTTON_CHANGE = 257;
    public static final int MAVLINK_MSG_LENGTH = 9;
    private static final long serialVersionUID = 257;
    public long last_change_ms;
    public short state;
    public long time_boot_ms;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(9, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = 257;
        mAVLinkPacket.payload.putUnsignedInt(this.time_boot_ms);
        mAVLinkPacket.payload.putUnsignedInt(this.last_change_ms);
        mAVLinkPacket.payload.putUnsignedByte(this.state);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_boot_ms = mAVLinkPayload.getUnsignedInt();
        this.last_change_ms = mAVLinkPayload.getUnsignedInt();
        this.state = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_button_change() {
        this.msgid = 257;
    }

    public msg_button_change(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = 257;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_BUTTON_CHANGE - sysid:" + this.sysid + " compid:" + this.compid + " time_boot_ms:" + this.time_boot_ms + " last_change_ms:" + this.last_change_ms + " state:" + this.state + "";
    }
}
