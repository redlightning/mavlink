package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_storage_information extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_STORAGE_INFORMATION = 261;
    public static final int MAVLINK_MSG_LENGTH = 26;
    private static final long serialVersionUID = 261;
    public float available_capacity;
    public float read_speed;
    public short status;
    public short storage_id;
    public long time_boot_ms;
    public float total_capacity;
    public float used_capacity;
    public float write_speed;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(26, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_STORAGE_INFORMATION;
        mAVLinkPacket.payload.putUnsignedInt(this.time_boot_ms);
        mAVLinkPacket.payload.putFloat(this.total_capacity);
        mAVLinkPacket.payload.putFloat(this.used_capacity);
        mAVLinkPacket.payload.putFloat(this.available_capacity);
        mAVLinkPacket.payload.putFloat(this.read_speed);
        mAVLinkPacket.payload.putFloat(this.write_speed);
        mAVLinkPacket.payload.putUnsignedByte(this.storage_id);
        mAVLinkPacket.payload.putUnsignedByte(this.status);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_boot_ms = mAVLinkPayload.getUnsignedInt();
        this.total_capacity = mAVLinkPayload.getFloat();
        this.used_capacity = mAVLinkPayload.getFloat();
        this.available_capacity = mAVLinkPayload.getFloat();
        this.read_speed = mAVLinkPayload.getFloat();
        this.write_speed = mAVLinkPayload.getFloat();
        this.storage_id = mAVLinkPayload.getUnsignedByte();
        this.status = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_storage_information() {
        this.msgid = MAVLINK_MSG_ID_STORAGE_INFORMATION;
    }

    public msg_storage_information(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_STORAGE_INFORMATION;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_STORAGE_INFORMATION - sysid:" + this.sysid + " compid:" + this.compid + " time_boot_ms:" + this.time_boot_ms + " total_capacity:" + this.total_capacity + " used_capacity:" + this.used_capacity + " available_capacity:" + this.available_capacity + " read_speed:" + this.read_speed + " write_speed:" + this.write_speed + " storage_id:" + this.storage_id + " status:" + this.status + "";
    }
}
