package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_uavcan_node_info extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_UAVCAN_NODE_INFO = 311;
    public static final int MAVLINK_MSG_LENGTH = 116;
    private static final long serialVersionUID = 311;
    public short[] hw_unique_id = new short[16];
    public short hw_version_major;
    public short hw_version_minor;
    public byte[] name = new byte[80];
    public long sw_vcs_commit;
    public short sw_version_major;
    public short sw_version_minor;
    public long time_usec;
    public long uptime_sec;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(116, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_UAVCAN_NODE_INFO;
        mAVLinkPacket.payload.putUnsignedLong(this.time_usec);
        mAVLinkPacket.payload.putUnsignedInt(this.uptime_sec);
        mAVLinkPacket.payload.putUnsignedInt(this.sw_vcs_commit);
        for (byte putByte : this.name) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        mAVLinkPacket.payload.putUnsignedByte(this.hw_version_major);
        mAVLinkPacket.payload.putUnsignedByte(this.hw_version_minor);
        for (short putUnsignedByte : this.hw_unique_id) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte);
        }
        mAVLinkPacket.payload.putUnsignedByte(this.sw_version_major);
        mAVLinkPacket.payload.putUnsignedByte(this.sw_version_minor);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_usec = mAVLinkPayload.getUnsignedLong();
        this.uptime_sec = mAVLinkPayload.getUnsignedInt();
        this.sw_vcs_commit = mAVLinkPayload.getUnsignedInt();
        for (int i = 0; i < this.name.length; i++) {
            this.name[i] = mAVLinkPayload.getByte();
        }
        this.hw_version_major = mAVLinkPayload.getUnsignedByte();
        this.hw_version_minor = mAVLinkPayload.getUnsignedByte();
        for (int i2 = 0; i2 < this.hw_unique_id.length; i2++) {
            this.hw_unique_id[i2] = mAVLinkPayload.getUnsignedByte();
        }
        this.sw_version_major = mAVLinkPayload.getUnsignedByte();
        this.sw_version_minor = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_uavcan_node_info() {
        this.msgid = MAVLINK_MSG_ID_UAVCAN_NODE_INFO;
    }

    public msg_uavcan_node_info(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_UAVCAN_NODE_INFO;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setName(String str) {
        int min = Math.min(str.length(), 80);
        for (int i = 0; i < min; i++) {
            this.name[i] = (byte) str.charAt(i);
        }
        while (min < 80) {
            this.name[min] = 0;
            min++;
        }
    }

    public String getName() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 80 && this.name[i] != 0) {
            stringBuffer.append((char) this.name[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_UAVCAN_NODE_INFO - sysid:" + this.sysid + " compid:" + this.compid + " time_usec:" + this.time_usec + " uptime_sec:" + this.uptime_sec + " sw_vcs_commit:" + this.sw_vcs_commit + " name:" + this.name + " hw_version_major:" + this.hw_version_major + " hw_version_minor:" + this.hw_version_minor + " hw_unique_id:" + this.hw_unique_id + " sw_version_major:" + this.sw_version_major + " sw_version_minor:" + this.sw_version_minor + "";
    }
}
