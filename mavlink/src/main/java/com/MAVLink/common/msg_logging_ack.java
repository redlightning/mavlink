package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_logging_ack extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_LOGGING_ACK = 268;
    public static final int MAVLINK_MSG_LENGTH = 4;
    private static final long serialVersionUID = 268;
    public int sequence;
    public short target_component;
    public short target_system;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(4, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_LOGGING_ACK;
        mAVLinkPacket.payload.putUnsignedShort(this.sequence);
        mAVLinkPacket.payload.putUnsignedByte(this.target_system);
        mAVLinkPacket.payload.putUnsignedByte(this.target_component);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.sequence = mAVLinkPayload.getUnsignedShort();
        this.target_system = mAVLinkPayload.getUnsignedByte();
        this.target_component = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_logging_ack() {
        this.msgid = MAVLINK_MSG_ID_LOGGING_ACK;
    }

    public msg_logging_ack(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_LOGGING_ACK;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_LOGGING_ACK - sysid:" + this.sysid + " compid:" + this.compid + " sequence:" + this.sequence + " target_system:" + this.target_system + " target_component:" + this.target_component + "";
    }
}
