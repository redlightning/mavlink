package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_camera_capture_status extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_CAMERA_CAPTURE_STATUS = 262;
    public static final int MAVLINK_MSG_LENGTH = 31;
    private static final long serialVersionUID = 262;
    public float available_capacity;
    public short camera_id;
    public float image_interval;
    public int image_resolution_h;
    public int image_resolution_v;
    public short image_status;
    public long recording_time_ms;
    public long time_boot_ms;
    public float video_framerate;
    public int video_resolution_h;
    public int video_resolution_v;
    public short video_status;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(31, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_CAMERA_CAPTURE_STATUS;
        mAVLinkPacket.payload.putUnsignedInt(this.time_boot_ms);
        mAVLinkPacket.payload.putFloat(this.image_interval);
        mAVLinkPacket.payload.putFloat(this.video_framerate);
        mAVLinkPacket.payload.putUnsignedInt(this.recording_time_ms);
        mAVLinkPacket.payload.putFloat(this.available_capacity);
        mAVLinkPacket.payload.putUnsignedShort(this.image_resolution_h);
        mAVLinkPacket.payload.putUnsignedShort(this.image_resolution_v);
        mAVLinkPacket.payload.putUnsignedShort(this.video_resolution_h);
        mAVLinkPacket.payload.putUnsignedShort(this.video_resolution_v);
        mAVLinkPacket.payload.putUnsignedByte(this.camera_id);
        mAVLinkPacket.payload.putUnsignedByte(this.image_status);
        mAVLinkPacket.payload.putUnsignedByte(this.video_status);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_boot_ms = mAVLinkPayload.getUnsignedInt();
        this.image_interval = mAVLinkPayload.getFloat();
        this.video_framerate = mAVLinkPayload.getFloat();
        this.recording_time_ms = mAVLinkPayload.getUnsignedInt();
        this.available_capacity = mAVLinkPayload.getFloat();
        this.image_resolution_h = mAVLinkPayload.getUnsignedShort();
        this.image_resolution_v = mAVLinkPayload.getUnsignedShort();
        this.video_resolution_h = mAVLinkPayload.getUnsignedShort();
        this.video_resolution_v = mAVLinkPayload.getUnsignedShort();
        this.camera_id = mAVLinkPayload.getUnsignedByte();
        this.image_status = mAVLinkPayload.getUnsignedByte();
        this.video_status = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_camera_capture_status() {
        this.msgid = MAVLINK_MSG_ID_CAMERA_CAPTURE_STATUS;
    }

    public msg_camera_capture_status(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_CAMERA_CAPTURE_STATUS;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_CAMERA_CAPTURE_STATUS - sysid:" + this.sysid + " compid:" + this.compid + " time_boot_ms:" + this.time_boot_ms + " image_interval:" + this.image_interval + " video_framerate:" + this.video_framerate + " recording_time_ms:" + this.recording_time_ms + " available_capacity:" + this.available_capacity + " image_resolution_h:" + this.image_resolution_h + " image_resolution_v:" + this.image_resolution_v + " video_resolution_h:" + this.video_resolution_h + " video_resolution_v:" + this.video_resolution_v + " camera_id:" + this.camera_id + " image_status:" + this.image_status + " video_status:" + this.video_status + "";
    }
}
