package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_obstacle_distance extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_OBSTACLE_DISTANCE = 330;
    public static final int MAVLINK_MSG_LENGTH = 158;
    private static final long serialVersionUID = 330;
    public int[] distances = new int[72];
    public short increment;
    public int max_distance;
    public int min_distance;
    public short sensor_type;
    public long time_usec;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(158, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_OBSTACLE_DISTANCE;
        mAVLinkPacket.payload.putUnsignedLong(this.time_usec);
        for (int putUnsignedShort : this.distances) {
            mAVLinkPacket.payload.putUnsignedShort(putUnsignedShort);
        }
        mAVLinkPacket.payload.putUnsignedShort(this.min_distance);
        mAVLinkPacket.payload.putUnsignedShort(this.max_distance);
        mAVLinkPacket.payload.putUnsignedByte(this.sensor_type);
        mAVLinkPacket.payload.putUnsignedByte(this.increment);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_usec = mAVLinkPayload.getUnsignedLong();
        for (int i = 0; i < this.distances.length; i++) {
            this.distances[i] = mAVLinkPayload.getUnsignedShort();
        }
        this.min_distance = mAVLinkPayload.getUnsignedShort();
        this.max_distance = mAVLinkPayload.getUnsignedShort();
        this.sensor_type = mAVLinkPayload.getUnsignedByte();
        this.increment = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_obstacle_distance() {
        this.msgid = MAVLINK_MSG_ID_OBSTACLE_DISTANCE;
    }

    public msg_obstacle_distance(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_OBSTACLE_DISTANCE;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_OBSTACLE_DISTANCE - sysid:" + this.sysid + " compid:" + this.compid + " time_usec:" + this.time_usec + " distances:" + this.distances + " min_distance:" + this.min_distance + " max_distance:" + this.max_distance + " sensor_type:" + this.sensor_type + " increment:" + this.increment + "";
    }
}
