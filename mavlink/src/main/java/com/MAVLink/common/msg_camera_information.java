package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_camera_information extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_CAMERA_INFORMATION = 259;
    public static final int MAVLINK_MSG_LENGTH = 86;
    private static final long serialVersionUID = 259;
    public short camera_id;
    public float focal_length;
    public short lense_id;
    public short[] model_name = new short[32];
    public int resolution_h;
    public int resolution_v;
    public float sensor_size_h;
    public float sensor_size_v;
    public long time_boot_ms;
    public short[] vendor_name = new short[32];

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(86, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_CAMERA_INFORMATION;
        mAVLinkPacket.payload.putUnsignedInt(this.time_boot_ms);
        mAVLinkPacket.payload.putFloat(this.focal_length);
        mAVLinkPacket.payload.putFloat(this.sensor_size_h);
        mAVLinkPacket.payload.putFloat(this.sensor_size_v);
        mAVLinkPacket.payload.putUnsignedShort(this.resolution_h);
        mAVLinkPacket.payload.putUnsignedShort(this.resolution_v);
        mAVLinkPacket.payload.putUnsignedByte(this.camera_id);
        for (short putUnsignedByte : this.vendor_name) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte);
        }
        for (short putUnsignedByte2 : this.model_name) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte2);
        }
        mAVLinkPacket.payload.putUnsignedByte(this.lense_id);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_boot_ms = mAVLinkPayload.getUnsignedInt();
        this.focal_length = mAVLinkPayload.getFloat();
        this.sensor_size_h = mAVLinkPayload.getFloat();
        this.sensor_size_v = mAVLinkPayload.getFloat();
        this.resolution_h = mAVLinkPayload.getUnsignedShort();
        this.resolution_v = mAVLinkPayload.getUnsignedShort();
        this.camera_id = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.vendor_name.length; i++) {
            this.vendor_name[i] = mAVLinkPayload.getUnsignedByte();
        }
        for (int i2 = 0; i2 < this.model_name.length; i2++) {
            this.model_name[i2] = mAVLinkPayload.getUnsignedByte();
        }
        this.lense_id = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_camera_information() {
        this.msgid = MAVLINK_MSG_ID_CAMERA_INFORMATION;
    }

    public msg_camera_information(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_CAMERA_INFORMATION;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_CAMERA_INFORMATION - sysid:" + this.sysid + " compid:" + this.compid + " time_boot_ms:" + this.time_boot_ms + " focal_length:" + this.focal_length + " sensor_size_h:" + this.sensor_size_h + " sensor_size_v:" + this.sensor_size_v + " resolution_h:" + this.resolution_h + " resolution_v:" + this.resolution_v + " camera_id:" + this.camera_id + " vendor_name:" + this.vendor_name + " model_name:" + this.model_name + " lense_id:" + this.lense_id + "";
    }
}
