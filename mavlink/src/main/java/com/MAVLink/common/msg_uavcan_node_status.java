package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_uavcan_node_status extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_UAVCAN_NODE_STATUS = 310;
    public static final int MAVLINK_MSG_LENGTH = 17;
    private static final long serialVersionUID = 310;
    public short health;
    public short mode;
    public short sub_mode;
    public long time_usec;
    public long uptime_sec;
    public int vendor_specific_status_code;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(17, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_UAVCAN_NODE_STATUS;
        mAVLinkPacket.payload.putUnsignedLong(this.time_usec);
        mAVLinkPacket.payload.putUnsignedInt(this.uptime_sec);
        mAVLinkPacket.payload.putUnsignedShort(this.vendor_specific_status_code);
        mAVLinkPacket.payload.putUnsignedByte(this.health);
        mAVLinkPacket.payload.putUnsignedByte(this.mode);
        mAVLinkPacket.payload.putUnsignedByte(this.sub_mode);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_usec = mAVLinkPayload.getUnsignedLong();
        this.uptime_sec = mAVLinkPayload.getUnsignedInt();
        this.vendor_specific_status_code = mAVLinkPayload.getUnsignedShort();
        this.health = mAVLinkPayload.getUnsignedByte();
        this.mode = mAVLinkPayload.getUnsignedByte();
        this.sub_mode = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_uavcan_node_status() {
        this.msgid = MAVLINK_MSG_ID_UAVCAN_NODE_STATUS;
    }

    public msg_uavcan_node_status(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_UAVCAN_NODE_STATUS;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_UAVCAN_NODE_STATUS - sysid:" + this.sysid + " compid:" + this.compid + " time_usec:" + this.time_usec + " uptime_sec:" + this.uptime_sec + " vendor_specific_status_code:" + this.vendor_specific_status_code + " health:" + this.health + " mode:" + this.mode + " sub_mode:" + this.sub_mode + "";
    }
}
