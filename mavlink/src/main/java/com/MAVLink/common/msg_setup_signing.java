package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_setup_signing extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_SETUP_SIGNING = 256;
    public static final int MAVLINK_MSG_LENGTH = 42;
    private static final long serialVersionUID = 256;
    public long initial_timestamp;
    public short[] secret_key = new short[32];
    public short target_component;
    public short target_system;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(42, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = 256;
        mAVLinkPacket.payload.putUnsignedLong(this.initial_timestamp);
        mAVLinkPacket.payload.putUnsignedByte(this.target_system);
        mAVLinkPacket.payload.putUnsignedByte(this.target_component);
        for (short putUnsignedByte : this.secret_key) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.initial_timestamp = mAVLinkPayload.getUnsignedLong();
        this.target_system = mAVLinkPayload.getUnsignedByte();
        this.target_component = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.secret_key.length; i++) {
            this.secret_key[i] = mAVLinkPayload.getUnsignedByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_setup_signing() {
        this.msgid = 256;
    }

    public msg_setup_signing(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = 256;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_SETUP_SIGNING - sysid:" + this.sysid + " compid:" + this.compid + " initial_timestamp:" + this.initial_timestamp + " target_system:" + this.target_system + " target_component:" + this.target_component + " secret_key:" + this.secret_key + "";
    }
}
