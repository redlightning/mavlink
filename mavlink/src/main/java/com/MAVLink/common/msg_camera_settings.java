package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_camera_settings extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_CAMERA_SETTINGS = 260;
    public static final int MAVLINK_MSG_LENGTH = 28;
    private static final long serialVersionUID = 260;
    public float aperture;
    public short aperture_locked;
    public short camera_id;
    public short color_mode_id;
    public short image_format_id;
    public float iso_sensitivity;
    public short iso_sensitivity_locked;
    public short mode_id;
    public float shutter_speed;
    public short shutter_speed_locked;
    public long time_boot_ms;
    public float white_balance;
    public short white_balance_locked;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(28, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_CAMERA_SETTINGS;
        mAVLinkPacket.payload.putUnsignedInt(this.time_boot_ms);
        mAVLinkPacket.payload.putFloat(this.aperture);
        mAVLinkPacket.payload.putFloat(this.shutter_speed);
        mAVLinkPacket.payload.putFloat(this.iso_sensitivity);
        mAVLinkPacket.payload.putFloat(this.white_balance);
        mAVLinkPacket.payload.putUnsignedByte(this.camera_id);
        mAVLinkPacket.payload.putUnsignedByte(this.aperture_locked);
        mAVLinkPacket.payload.putUnsignedByte(this.shutter_speed_locked);
        mAVLinkPacket.payload.putUnsignedByte(this.iso_sensitivity_locked);
        mAVLinkPacket.payload.putUnsignedByte(this.white_balance_locked);
        mAVLinkPacket.payload.putUnsignedByte(this.mode_id);
        mAVLinkPacket.payload.putUnsignedByte(this.color_mode_id);
        mAVLinkPacket.payload.putUnsignedByte(this.image_format_id);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_boot_ms = mAVLinkPayload.getUnsignedInt();
        this.aperture = mAVLinkPayload.getFloat();
        this.shutter_speed = mAVLinkPayload.getFloat();
        this.iso_sensitivity = mAVLinkPayload.getFloat();
        this.white_balance = mAVLinkPayload.getFloat();
        this.camera_id = mAVLinkPayload.getUnsignedByte();
        this.aperture_locked = mAVLinkPayload.getUnsignedByte();
        this.shutter_speed_locked = mAVLinkPayload.getUnsignedByte();
        this.iso_sensitivity_locked = mAVLinkPayload.getUnsignedByte();
        this.white_balance_locked = mAVLinkPayload.getUnsignedByte();
        this.mode_id = mAVLinkPayload.getUnsignedByte();
        this.color_mode_id = mAVLinkPayload.getUnsignedByte();
        this.image_format_id = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_camera_settings() {
        this.msgid = MAVLINK_MSG_ID_CAMERA_SETTINGS;
    }

    public msg_camera_settings(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_CAMERA_SETTINGS;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_CAMERA_SETTINGS - sysid:" + this.sysid + " compid:" + this.compid + " time_boot_ms:" + this.time_boot_ms + " aperture:" + this.aperture + " shutter_speed:" + this.shutter_speed + " iso_sensitivity:" + this.iso_sensitivity + " white_balance:" + this.white_balance + " camera_id:" + this.camera_id + " aperture_locked:" + this.aperture_locked + " shutter_speed_locked:" + this.shutter_speed_locked + " iso_sensitivity_locked:" + this.iso_sensitivity_locked + " white_balance_locked:" + this.white_balance_locked + " mode_id:" + this.mode_id + " color_mode_id:" + this.color_mode_id + " image_format_id:" + this.image_format_id + "";
    }
}
