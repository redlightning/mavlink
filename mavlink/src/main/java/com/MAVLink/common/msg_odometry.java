package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_odometry extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_ODOMETRY = 331;
    public static final int MAVLINK_MSG_LENGTH = 230;
    private static final long serialVersionUID = 331;
    public short child_frame_id;
    public short frame_id;
    public float pitchspeed;
    public float[] pose_covariance = new float[21];
    public float[] q = new float[4];
    public float rollspeed;
    public long time_usec;
    public float[] twist_covariance = new float[21];
    public float vx;
    public float vy;
    public float vz;
    public float x;
    public float y;
    public float yawspeed;
    public float z;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(230, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_ODOMETRY;
        mAVLinkPacket.payload.putUnsignedLong(this.time_usec);
        mAVLinkPacket.payload.putFloat(this.x);
        mAVLinkPacket.payload.putFloat(this.y);
        mAVLinkPacket.payload.putFloat(this.z);
        for (float putFloat : this.q) {
            mAVLinkPacket.payload.putFloat(putFloat);
        }
        mAVLinkPacket.payload.putFloat(this.vx);
        mAVLinkPacket.payload.putFloat(this.vy);
        mAVLinkPacket.payload.putFloat(this.vz);
        mAVLinkPacket.payload.putFloat(this.rollspeed);
        mAVLinkPacket.payload.putFloat(this.pitchspeed);
        mAVLinkPacket.payload.putFloat(this.yawspeed);
        for (float putFloat2 : this.pose_covariance) {
            mAVLinkPacket.payload.putFloat(putFloat2);
        }
        for (float putFloat3 : this.twist_covariance) {
            mAVLinkPacket.payload.putFloat(putFloat3);
        }
        mAVLinkPacket.payload.putUnsignedByte(this.frame_id);
        mAVLinkPacket.payload.putUnsignedByte(this.child_frame_id);
        boolean z2 = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_usec = mAVLinkPayload.getUnsignedLong();
        this.x = mAVLinkPayload.getFloat();
        this.y = mAVLinkPayload.getFloat();
        this.z = mAVLinkPayload.getFloat();
        for (int i = 0; i < this.q.length; i++) {
            this.q[i] = mAVLinkPayload.getFloat();
        }
        this.vx = mAVLinkPayload.getFloat();
        this.vy = mAVLinkPayload.getFloat();
        this.vz = mAVLinkPayload.getFloat();
        this.rollspeed = mAVLinkPayload.getFloat();
        this.pitchspeed = mAVLinkPayload.getFloat();
        this.yawspeed = mAVLinkPayload.getFloat();
        for (int i2 = 0; i2 < this.pose_covariance.length; i2++) {
            this.pose_covariance[i2] = mAVLinkPayload.getFloat();
        }
        for (int i3 = 0; i3 < this.twist_covariance.length; i3++) {
            this.twist_covariance[i3] = mAVLinkPayload.getFloat();
        }
        this.frame_id = mAVLinkPayload.getUnsignedByte();
        this.child_frame_id = mAVLinkPayload.getUnsignedByte();
        boolean z2 = this.isMavlink2;
    }

    public msg_odometry() {
        this.msgid = MAVLINK_MSG_ID_ODOMETRY;
    }

    public msg_odometry(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_ODOMETRY;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_ODOMETRY - sysid:" + this.sysid + " compid:" + this.compid + " time_usec:" + this.time_usec + " x:" + this.x + " y:" + this.y + " z:" + this.z + " q:" + this.q + " vx:" + this.vx + " vy:" + this.vy + " vz:" + this.vz + " rollspeed:" + this.rollspeed + " pitchspeed:" + this.pitchspeed + " yawspeed:" + this.yawspeed + " pose_covariance:" + this.pose_covariance + " twist_covariance:" + this.twist_covariance + " frame_id:" + this.frame_id + " child_frame_id:" + this.child_frame_id + "";
    }
}
