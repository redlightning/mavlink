package com.MAVLink.common;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_wifi_config_ap extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_WIFI_CONFIG_AP = 299;
    public static final int MAVLINK_MSG_LENGTH = 96;
    private static final long serialVersionUID = 299;
    public byte[] password = new byte[64];
    public byte[] ssid = new byte[32];

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(96, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_WIFI_CONFIG_AP;
        for (byte putByte : this.ssid) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        for (byte putByte2 : this.password) {
            mAVLinkPacket.payload.putByte(putByte2);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        for (int i = 0; i < this.ssid.length; i++) {
            this.ssid[i] = mAVLinkPayload.getByte();
        }
        for (int i2 = 0; i2 < this.password.length; i2++) {
            this.password[i2] = mAVLinkPayload.getByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_wifi_config_ap() {
        this.msgid = MAVLINK_MSG_ID_WIFI_CONFIG_AP;
    }

    public msg_wifi_config_ap(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_WIFI_CONFIG_AP;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setSsid(String str) {
        int min = Math.min(str.length(), 32);
        for (int i = 0; i < min; i++) {
            this.ssid[i] = (byte) str.charAt(i);
        }
        while (min < 32) {
            this.ssid[min] = 0;
            min++;
        }
    }

    public String getSsid() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 32 && this.ssid[i] != 0) {
            stringBuffer.append((char) this.ssid[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public void setPassword(String str) {
        int min = Math.min(str.length(), 64);
        for (int i = 0; i < min; i++) {
            this.password[i] = (byte) str.charAt(i);
        }
        while (min < 64) {
            this.password[min] = 0;
            min++;
        }
    }

    public String getPassword() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 64 && this.password[i] != 0) {
            stringBuffer.append((char) this.password[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_WIFI_CONFIG_AP - sysid:" + this.sysid + " compid:" + this.compid + " ssid:" + this.ssid + " password:" + this.password + "";
    }
}
