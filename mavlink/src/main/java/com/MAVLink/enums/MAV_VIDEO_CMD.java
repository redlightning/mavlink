package com.MAVLink.enums;

public class MAV_VIDEO_CMD {
    public static final int MAV_CMD_IMAGE_START_CAPTURE = 2000;
    public static final int MAV_CMD_IMAGE_STOP_CAPTURE = 2001;
    public static final int MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS = 527;
    public static final int MAV_CMD_REQUEST_CAMERA_IMAGE_CAPTURE = 2002;
    public static final int MAV_CMD_REQUEST_CAMERA_INFORMATION = 521;
    public static final int MAV_CMD_REQUEST_CAMERA_SETTINGS = 522;
    public static final int MAV_CMD_REQUEST_STORAGE_INFORMATION = 525;
    public static final int MAV_CMD_REQUEST_VIDEO_STREAM_INFORMATION = 2504;
    public static final int MAV_CMD_REQUEST_VIDEO_STREAM_STATUS = 2505;
    public static final int MAV_CMD_RESET_CAMERA_SETTINGS = 529;
    public static final int MAV_CMD_SET_CAMERA_MODE = 530;
    public static final int MAV_CMD_STORAGE_FORMAT = 526;
    public static final int MAV_CMD_VIDEO_START_STREAMING = 2502;
    public static final int MAV_CMD_VIDEO_STOP_STREAMING = 2503;
    public static final int MAV_VIDEO_CMD_ENUM_END = 2506;
}
