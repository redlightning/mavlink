package com.MAVLink.ardupilotmega;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_device_op_read_reply extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_DEVICE_OP_READ_REPLY = 11001;
    public static final int MAVLINK_MSG_LENGTH = 135;
    private static final long serialVersionUID = 11001;
    public short count;
    public short[] data = new short[128];
    public short regstart;
    public long request_id;
    public short result;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(135, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_DEVICE_OP_READ_REPLY;
        mAVLinkPacket.payload.putUnsignedInt(this.request_id);
        mAVLinkPacket.payload.putUnsignedByte(this.result);
        mAVLinkPacket.payload.putUnsignedByte(this.regstart);
        mAVLinkPacket.payload.putUnsignedByte(this.count);
        for (short putUnsignedByte : this.data) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.request_id = mAVLinkPayload.getUnsignedInt();
        this.result = mAVLinkPayload.getUnsignedByte();
        this.regstart = mAVLinkPayload.getUnsignedByte();
        this.count = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = mAVLinkPayload.getUnsignedByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_device_op_read_reply() {
        this.msgid = MAVLINK_MSG_ID_DEVICE_OP_READ_REPLY;
    }

    public msg_device_op_read_reply(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_DEVICE_OP_READ_REPLY;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_DEVICE_OP_READ_REPLY - sysid:" + this.sysid + " compid:" + this.compid + " request_id:" + this.request_id + " result:" + this.result + " regstart:" + this.regstart + " count:" + this.count + " data:" + this.data + "";
    }
}
