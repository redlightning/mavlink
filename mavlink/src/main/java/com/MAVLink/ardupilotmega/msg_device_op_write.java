package com.MAVLink.ardupilotmega;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_device_op_write extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_DEVICE_OP_WRITE = 11002;
    public static final int MAVLINK_MSG_LENGTH = 179;
    private static final long serialVersionUID = 11002;
    public short address;
    public short bus;
    public byte[] busname = new byte[40];
    public short bustype;
    public short count;
    public short[] data = new short[128];
    public short regstart;
    public long request_id;
    public short target_component;
    public short target_system;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(179, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_DEVICE_OP_WRITE;
        mAVLinkPacket.payload.putUnsignedInt(this.request_id);
        mAVLinkPacket.payload.putUnsignedByte(this.target_system);
        mAVLinkPacket.payload.putUnsignedByte(this.target_component);
        mAVLinkPacket.payload.putUnsignedByte(this.bustype);
        mAVLinkPacket.payload.putUnsignedByte(this.bus);
        mAVLinkPacket.payload.putUnsignedByte(this.address);
        for (byte putByte : this.busname) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        mAVLinkPacket.payload.putUnsignedByte(this.regstart);
        mAVLinkPacket.payload.putUnsignedByte(this.count);
        for (short putUnsignedByte : this.data) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.request_id = mAVLinkPayload.getUnsignedInt();
        this.target_system = mAVLinkPayload.getUnsignedByte();
        this.target_component = mAVLinkPayload.getUnsignedByte();
        this.bustype = mAVLinkPayload.getUnsignedByte();
        this.bus = mAVLinkPayload.getUnsignedByte();
        this.address = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.busname.length; i++) {
            this.busname[i] = mAVLinkPayload.getByte();
        }
        this.regstart = mAVLinkPayload.getUnsignedByte();
        this.count = mAVLinkPayload.getUnsignedByte();
        for (int i2 = 0; i2 < this.data.length; i2++) {
            this.data[i2] = mAVLinkPayload.getUnsignedByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_device_op_write() {
        this.msgid = MAVLINK_MSG_ID_DEVICE_OP_WRITE;
    }

    public msg_device_op_write(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_DEVICE_OP_WRITE;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setBusname(String str) {
        int min = Math.min(str.length(), 40);
        for (int i = 0; i < min; i++) {
            this.busname[i] = (byte) str.charAt(i);
        }
        while (min < 40) {
            this.busname[min] = 0;
            min++;
        }
    }

    public String getBusname() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 40 && this.busname[i] != 0) {
            stringBuffer.append((char) this.busname[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_DEVICE_OP_WRITE - sysid:" + this.sysid + " compid:" + this.compid + " request_id:" + this.request_id + " target_system:" + this.target_system + " target_component:" + this.target_component + " bustype:" + this.bustype + " bus:" + this.bus + " address:" + this.address + " busname:" + this.busname + " regstart:" + this.regstart + " count:" + this.count + " data:" + this.data + "";
    }
}
