package com.MAVLink.ardupilotmega;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_esc_telemetry_5_to_8 extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_ESC_TELEMETRY_5_TO_8 = 11031;
    public static final int MAVLINK_MSG_LENGTH = 44;
    private static final long serialVersionUID = 11031;
    public int[] count = new int[4];
    public int[] current = new int[4];
    public int[] rpm = new int[4];
    public short[] temperature = new short[4];
    public int[] totalcurrent = new int[4];
    public int[] voltage = new int[4];

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(44, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_ESC_TELEMETRY_5_TO_8;
        for (int putUnsignedShort : this.voltage) {
            mAVLinkPacket.payload.putUnsignedShort(putUnsignedShort);
        }
        for (int putUnsignedShort2 : this.current) {
            mAVLinkPacket.payload.putUnsignedShort(putUnsignedShort2);
        }
        for (int putUnsignedShort3 : this.totalcurrent) {
            mAVLinkPacket.payload.putUnsignedShort(putUnsignedShort3);
        }
        for (int putUnsignedShort4 : this.rpm) {
            mAVLinkPacket.payload.putUnsignedShort(putUnsignedShort4);
        }
        for (int putUnsignedShort5 : this.count) {
            mAVLinkPacket.payload.putUnsignedShort(putUnsignedShort5);
        }
        for (short putUnsignedByte : this.temperature) {
            mAVLinkPacket.payload.putUnsignedByte(putUnsignedByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        for (int i = 0; i < this.voltage.length; i++) {
            this.voltage[i] = mAVLinkPayload.getUnsignedShort();
        }
        for (int i2 = 0; i2 < this.current.length; i2++) {
            this.current[i2] = mAVLinkPayload.getUnsignedShort();
        }
        for (int i3 = 0; i3 < this.totalcurrent.length; i3++) {
            this.totalcurrent[i3] = mAVLinkPayload.getUnsignedShort();
        }
        for (int i4 = 0; i4 < this.rpm.length; i4++) {
            this.rpm[i4] = mAVLinkPayload.getUnsignedShort();
        }
        for (int i5 = 0; i5 < this.count.length; i5++) {
            this.count[i5] = mAVLinkPayload.getUnsignedShort();
        }
        for (int i6 = 0; i6 < this.temperature.length; i6++) {
            this.temperature[i6] = mAVLinkPayload.getUnsignedByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_esc_telemetry_5_to_8() {
        this.msgid = MAVLINK_MSG_ID_ESC_TELEMETRY_5_TO_8;
    }

    public msg_esc_telemetry_5_to_8(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_ESC_TELEMETRY_5_TO_8;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_ESC_TELEMETRY_5_TO_8 - sysid:" + this.sysid + " compid:" + this.compid + " voltage:" + this.voltage + " current:" + this.current + " totalcurrent:" + this.totalcurrent + " rpm:" + this.rpm + " count:" + this.count + " temperature:" + this.temperature + "";
    }
}
