package com.MAVLink.ardupilotmega;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

import org.jetbrains.annotations.NotNull;public class msg_aoa_ssa extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_AOA_SSA = 11020;
    public static final int MAVLINK_MSG_LENGTH = 16;
    private static final long serialVersionUID = 11020;
    public float AOA;
    public float SSA;
    public long time_usec;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(16, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_AOA_SSA;
        mAVLinkPacket.payload.putUnsignedLong(this.time_usec);
        mAVLinkPacket.payload.putFloat(this.AOA);
        mAVLinkPacket.payload.putFloat(this.SSA);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_usec = mAVLinkPayload.getUnsignedLong();
        this.AOA = mAVLinkPayload.getFloat();
        this.SSA = mAVLinkPayload.getFloat();
        boolean z = this.isMavlink2;
    }

    public msg_aoa_ssa() {
        this.msgid = MAVLINK_MSG_ID_AOA_SSA;
    }

    public msg_aoa_ssa(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_AOA_SSA;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    @NotNull
    public String toString() {
        return "MAVLINK_MSG_ID_AOA_SSA - sysid:" + this.sysid + " compid:" + this.compid + " time_usec:" + this.time_usec + " AOA:" + this.AOA + " SSA:" + this.SSA + "";
    }
}
