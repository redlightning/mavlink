package com.MAVLink.ardupilotmega;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_device_op_write_reply extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_DEVICE_OP_WRITE_REPLY = 11003;
    public static final int MAVLINK_MSG_LENGTH = 5;
    private static final long serialVersionUID = 11003;
    public long request_id;
    public short result;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(5, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_DEVICE_OP_WRITE_REPLY;
        mAVLinkPacket.payload.putUnsignedInt(this.request_id);
        mAVLinkPacket.payload.putUnsignedByte(this.result);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.request_id = mAVLinkPayload.getUnsignedInt();
        this.result = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_device_op_write_reply() {
        this.msgid = MAVLINK_MSG_ID_DEVICE_OP_WRITE_REPLY;
    }

    public msg_device_op_write_reply(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_DEVICE_OP_WRITE_REPLY;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_DEVICE_OP_WRITE_REPLY - sysid:" + this.sysid + " compid:" + this.compid + " request_id:" + this.request_id + " result:" + this.result + "";
    }
}
