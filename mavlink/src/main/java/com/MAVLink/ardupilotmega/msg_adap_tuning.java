package com.MAVLink.ardupilotmega;

import org.jetbrains.annotations.NotNull;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_adap_tuning extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_ADAP_TUNING = 11010;
    public static final int MAVLINK_MSG_LENGTH = 49;
    private static final long serialVersionUID = 11010;
    public float achieved;
    public short axis;
    public float desired;
    public float error;
    public float f;
    public float f_dot;
    public float omega;
    public float omega_dot;
    public float sigma;
    public float sigma_dot;
    public float theta;
    public float theta_dot;
    public float u;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(49, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_ADAP_TUNING;
        mAVLinkPacket.payload.putFloat(this.desired);
        mAVLinkPacket.payload.putFloat(this.achieved);
        mAVLinkPacket.payload.putFloat(this.error);
        mAVLinkPacket.payload.putFloat(this.theta);
        mAVLinkPacket.payload.putFloat(this.omega);
        mAVLinkPacket.payload.putFloat(this.sigma);
        mAVLinkPacket.payload.putFloat(this.theta_dot);
        mAVLinkPacket.payload.putFloat(this.omega_dot);
        mAVLinkPacket.payload.putFloat(this.sigma_dot);
        mAVLinkPacket.payload.putFloat(this.f);
        mAVLinkPacket.payload.putFloat(this.f_dot);
        mAVLinkPacket.payload.putFloat(this.u);
        mAVLinkPacket.payload.putUnsignedByte(this.axis);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.desired = mAVLinkPayload.getFloat();
        this.achieved = mAVLinkPayload.getFloat();
        this.error = mAVLinkPayload.getFloat();
        this.theta = mAVLinkPayload.getFloat();
        this.omega = mAVLinkPayload.getFloat();
        this.sigma = mAVLinkPayload.getFloat();
        this.theta_dot = mAVLinkPayload.getFloat();
        this.omega_dot = mAVLinkPayload.getFloat();
        this.sigma_dot = mAVLinkPayload.getFloat();
        this.f = mAVLinkPayload.getFloat();
        this.f_dot = mAVLinkPayload.getFloat();
        this.u = mAVLinkPayload.getFloat();
        this.axis = mAVLinkPayload.getUnsignedByte();
        boolean z = this.isMavlink2;
    }

    public msg_adap_tuning() {
        this.msgid = MAVLINK_MSG_ID_ADAP_TUNING;
    }

    public msg_adap_tuning(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_ADAP_TUNING;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    @NotNull
    public String toString() {
        return "MAVLINK_MSG_ID_ADAP_TUNING - sysid:" + this.sysid + " compid:" + this.compid + " desired:" + this.desired + " achieved:" + this.achieved + " error:" + this.error + " theta:" + this.theta + " omega:" + this.omega + " sigma:" + this.sigma + " theta_dot:" + this.theta_dot + " omega_dot:" + this.omega_dot + " sigma_dot:" + this.sigma_dot + " f:" + this.f + " f_dot:" + this.f_dot + " u:" + this.u + " axis:" + this.axis + "";
    }
}
