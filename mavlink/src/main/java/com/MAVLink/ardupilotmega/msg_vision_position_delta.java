package com.MAVLink.ardupilotmega;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_vision_position_delta extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_VISION_POSITION_DELTA = 11011;
    public static final int MAVLINK_MSG_LENGTH = 44;
    private static final long serialVersionUID = 11011;
    public float[] angle_delta = new float[3];
    public float confidence;
    public float[] position_delta = new float[3];
    public long time_delta_usec;
    public long time_usec;

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(44, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_VISION_POSITION_DELTA;
        mAVLinkPacket.payload.putUnsignedLong(this.time_usec);
        mAVLinkPacket.payload.putUnsignedLong(this.time_delta_usec);
        for (float putFloat : this.angle_delta) {
            mAVLinkPacket.payload.putFloat(putFloat);
        }
        for (float putFloat2 : this.position_delta) {
            mAVLinkPacket.payload.putFloat(putFloat2);
        }
        mAVLinkPacket.payload.putFloat(this.confidence);
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.time_usec = mAVLinkPayload.getUnsignedLong();
        this.time_delta_usec = mAVLinkPayload.getUnsignedLong();
        for (int i = 0; i < this.angle_delta.length; i++) {
            this.angle_delta[i] = mAVLinkPayload.getFloat();
        }
        for (int i2 = 0; i2 < this.position_delta.length; i2++) {
            this.position_delta[i2] = mAVLinkPayload.getFloat();
        }
        this.confidence = mAVLinkPayload.getFloat();
        boolean z = this.isMavlink2;
    }

    public msg_vision_position_delta() {
        this.msgid = MAVLINK_MSG_ID_VISION_POSITION_DELTA;
    }

    public msg_vision_position_delta(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_VISION_POSITION_DELTA;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public String toString() {
        return "MAVLINK_MSG_ID_VISION_POSITION_DELTA - sysid:" + this.sysid + " compid:" + this.compid + " time_usec:" + this.time_usec + " time_delta_usec:" + this.time_delta_usec + " angle_delta:" + this.angle_delta + " position_delta:" + this.position_delta + " confidence:" + this.confidence + "";
    }
}
