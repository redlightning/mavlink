package com.MAVLink.herelink_custom;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_set_video_stream_settings extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_SET_VIDEO_STREAM_SETTINGS = 270;
    public static final int MAVLINK_MSG_LENGTH = 247;
    private static final long serialVersionUID = 270;
    public long bitrate;
    public short camera_id;
    public float framerate;
    public int resolution_h;
    public int resolution_v;
    public int rotation;
    public short target_component;
    public short target_system;
    public byte[] uri = new byte[230];

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(247, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_SET_VIDEO_STREAM_SETTINGS;
        mAVLinkPacket.payload.putFloat(this.framerate);
        mAVLinkPacket.payload.putUnsignedInt(this.bitrate);
        mAVLinkPacket.payload.putUnsignedShort(this.resolution_h);
        mAVLinkPacket.payload.putUnsignedShort(this.resolution_v);
        mAVLinkPacket.payload.putUnsignedShort(this.rotation);
        mAVLinkPacket.payload.putUnsignedByte(this.target_system);
        mAVLinkPacket.payload.putUnsignedByte(this.target_component);
        mAVLinkPacket.payload.putUnsignedByte(this.camera_id);
        for (byte putByte : this.uri) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.framerate = mAVLinkPayload.getFloat();
        this.bitrate = mAVLinkPayload.getUnsignedInt();
        this.resolution_h = mAVLinkPayload.getUnsignedShort();
        this.resolution_v = mAVLinkPayload.getUnsignedShort();
        this.rotation = mAVLinkPayload.getUnsignedShort();
        this.target_system = mAVLinkPayload.getUnsignedByte();
        this.target_component = mAVLinkPayload.getUnsignedByte();
        this.camera_id = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.uri.length; i++) {
            this.uri[i] = mAVLinkPayload.getByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_set_video_stream_settings() {
        this.msgid = MAVLINK_MSG_ID_SET_VIDEO_STREAM_SETTINGS;
    }

    public msg_set_video_stream_settings(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_SET_VIDEO_STREAM_SETTINGS;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setUri(String str) {
        int min = Math.min(str.length(), 230);
        for (int i = 0; i < min; i++) {
            this.uri[i] = (byte) str.charAt(i);
        }
        while (min < 230) {
            this.uri[min] = 0;
            min++;
        }
    }

    public String getUri() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 230 && this.uri[i] != 0) {
            stringBuffer.append((char) this.uri[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_SET_VIDEO_STREAM_SETTINGS - sysid:" + this.sysid + " compid:" + this.compid + " framerate:" + this.framerate + " bitrate:" + this.bitrate + " resolution_h:" + this.resolution_h + " resolution_v:" + this.resolution_v + " rotation:" + this.rotation + " target_system:" + this.target_system + " target_component:" + this.target_component + " camera_id:" + this.camera_id + " uri:" + this.uri + "";
    }
}
