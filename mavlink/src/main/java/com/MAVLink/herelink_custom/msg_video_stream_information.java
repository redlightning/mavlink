package com.MAVLink.herelink_custom;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.MAVLink.Messages.MAVLinkPayload;

public class msg_video_stream_information extends MAVLinkMessage {
    public static final int MAVLINK_MSG_ID_VIDEO_STREAM_INFORMATION = 269;
    public static final int MAVLINK_MSG_LENGTH = 246;
    private static final long serialVersionUID = 269;
    public long bitrate;
    public short camera_id;
    public float framerate;
    public int resolution_h;
    public int resolution_v;
    public int rotation;
    public short status;
    public byte[] uri = new byte[230];

    public MAVLinkPacket pack() {
        MAVLinkPacket mAVLinkPacket = new MAVLinkPacket(246, this.isMavlink2);
        mAVLinkPacket.sysid = 255;
        mAVLinkPacket.compid = 190;
        mAVLinkPacket.msgid = MAVLINK_MSG_ID_VIDEO_STREAM_INFORMATION;
        mAVLinkPacket.payload.putFloat(this.framerate);
        mAVLinkPacket.payload.putUnsignedInt(this.bitrate);
        mAVLinkPacket.payload.putUnsignedShort(this.resolution_h);
        mAVLinkPacket.payload.putUnsignedShort(this.resolution_v);
        mAVLinkPacket.payload.putUnsignedShort(this.rotation);
        mAVLinkPacket.payload.putUnsignedByte(this.camera_id);
        mAVLinkPacket.payload.putUnsignedByte(this.status);
        for (byte putByte : this.uri) {
            mAVLinkPacket.payload.putByte(putByte);
        }
        boolean z = this.isMavlink2;
        return mAVLinkPacket;
    }

    public void unpack(MAVLinkPayload mAVLinkPayload) {
        mAVLinkPayload.resetIndex();
        this.framerate = mAVLinkPayload.getFloat();
        this.bitrate = mAVLinkPayload.getUnsignedInt();
        this.resolution_h = mAVLinkPayload.getUnsignedShort();
        this.resolution_v = mAVLinkPayload.getUnsignedShort();
        this.rotation = mAVLinkPayload.getUnsignedShort();
        this.camera_id = mAVLinkPayload.getUnsignedByte();
        this.status = mAVLinkPayload.getUnsignedByte();
        for (int i = 0; i < this.uri.length; i++) {
            this.uri[i] = mAVLinkPayload.getByte();
        }
        boolean z = this.isMavlink2;
    }

    public msg_video_stream_information() {
        this.msgid = MAVLINK_MSG_ID_VIDEO_STREAM_INFORMATION;
    }

    public msg_video_stream_information(MAVLinkPacket mAVLinkPacket) {
        this.sysid = mAVLinkPacket.sysid;
        this.compid = mAVLinkPacket.compid;
        this.msgid = MAVLINK_MSG_ID_VIDEO_STREAM_INFORMATION;
        this.isMavlink2 = mAVLinkPacket.isMavlink2;
        unpack(mAVLinkPacket.payload);
    }

    public void setUri(String str) {
        int min = Math.min(str.length(), 230);
        for (int i = 0; i < min; i++) {
            this.uri[i] = (byte) str.charAt(i);
        }
        while (min < 230) {
            this.uri[min] = 0;
            min++;
        }
    }

    public String getUri() {
        StringBuilder stringBuffer = new StringBuilder();
        int i = 0;
        while (i < 230 && this.uri[i] != 0) {
            stringBuffer.append((char) this.uri[i]);
            i++;
        }
        return stringBuffer.toString();
    }

    public String toString() {
        return "MAVLINK_MSG_ID_VIDEO_STREAM_INFORMATION - sysid:" + this.sysid + " compid:" + this.compid + " framerate:" + this.framerate + " bitrate:" + this.bitrate + " resolution_h:" + this.resolution_h + " resolution_v:" + this.resolution_v + " rotation:" + this.rotation + " camera_id:" + this.camera_id + " status:" + this.status + " uri:" + this.uri + "";
    }
}
