package com.MAVLink;

import com.MAVLink.Messages.MAVLinkStats;

import timber.log.Timber;

public class Parser {
    static final boolean V = false;
    private boolean isMavlink2;
    private MAVLinkPacket m;
    private MAV_states state;
    public MAVLinkStats stats;

    enum MAV_states {
        MAVLINK_PARSE_STATE_UNINIT,
        MAVLINK_PARSE_STATE_IDLE,
        MAVLINK_PARSE_STATE_GOT_STX,
        MAVLINK_PARSE_STATE_GOT_LENGTH,
        MAVLINK_PARSE_STATE_GOT_INCOMPAT_FLAGS,
        MAVLINK_PARSE_STATE_GOT_COMPAT_FLAGS,
        MAVLINK_PARSE_STATE_GOT_SEQ,
        MAVLINK_PARSE_STATE_GOT_SYSID,
        MAVLINK_PARSE_STATE_GOT_COMPID,
        MAVLINK_PARSE_STATE_GOT_MSGID1,
        MAVLINK_PARSE_STATE_GOT_MSGID2,
        MAVLINK_PARSE_STATE_GOT_MSGID3,
        MAVLINK_PARSE_STATE_GOT_CRC1,
        MAVLINK_PARSE_STATE_GOT_CRC2,
        MAVLINK_PARSE_STATE_GOT_PAYLOAD,
        MAVLINK_PARSE_STATE_GOT_SIGNATURE
    }

    public Parser() {
        this(false);
    }

    public Parser(boolean z) {
        this.state = MAV_states.MAVLINK_PARSE_STATE_UNINIT;
        this.stats = new MAVLinkStats(z);
        this.isMavlink2 = false;
    }

    public MAVLinkPacket mavlink_parse_char(int i) {
        int i2 = i & 255;
        switch (this.state) {
            case MAVLINK_PARSE_STATE_UNINIT:
            case MAVLINK_PARSE_STATE_IDLE:
                if (i2 == 253) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_STX;
                    if (this.isMavlink2) {
                        return null;
                    }
                    this.isMavlink2 = true;
                    Timber.v("Turning mavlink2 ON");
                    return null;
                } else if (i2 != 254) {
                    return null;
                } else {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_STX;
                    if (!this.isMavlink2) {
                        return null;
                    }
                    this.isMavlink2 = false;
                    Timber.v("Turning mavlink2 OFF");
                    return null;
                }
            case MAVLINK_PARSE_STATE_GOT_STX:
                this.m = new MAVLinkPacket(i2, this.isMavlink2);
                if (this.isMavlink2) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_LENGTH;
                    return null;
                }
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_COMPAT_FLAGS;
                return null;
            case MAVLINK_PARSE_STATE_GOT_LENGTH:
                this.m.incompatFlags = i2;
                if (i2 == 0 || i2 == 1) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_INCOMPAT_FLAGS;
                    return null;
                }
                this.state = MAV_states.MAVLINK_PARSE_STATE_IDLE;
                return null;
            case MAVLINK_PARSE_STATE_GOT_INCOMPAT_FLAGS:
                this.m.compatFlags = i2;
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_COMPAT_FLAGS;
                return null;
            case MAVLINK_PARSE_STATE_GOT_COMPAT_FLAGS:
                this.m.seq = i2;
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_SEQ;
                return null;
            case MAVLINK_PARSE_STATE_GOT_SEQ:
                this.m.sysid = i2;
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_SYSID;
                return null;
            case MAVLINK_PARSE_STATE_GOT_SYSID:
                this.m.compid = i2;
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_COMPID;
                return null;
            case MAVLINK_PARSE_STATE_GOT_COMPID:
                this.m.msgid = i2;
                if (this.isMavlink2) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_MSGID1;
                    return null;
                } else if (this.m.len > 0) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_MSGID3;
                    return null;
                } else {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_PAYLOAD;
                    return null;
                }
            case MAVLINK_PARSE_STATE_GOT_MSGID1:
                MAVLinkPacket mAVLinkPacket = this.m;
                mAVLinkPacket.msgid = (i2 << 8) | mAVLinkPacket.msgid;
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_MSGID2;
                return null;
            case MAVLINK_PARSE_STATE_GOT_MSGID2:
                MAVLinkPacket mAVLinkPacket2 = this.m;
                mAVLinkPacket2.msgid = (i2 << 16) | mAVLinkPacket2.msgid;
                if (this.m.len > 0) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_MSGID3;
                    return null;
                }
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_PAYLOAD;
                return null;
            case MAVLINK_PARSE_STATE_GOT_MSGID3:
                this.m.payload.add((byte) i2);
                if (!this.m.payloadIsFilled()) {
                    return null;
                }
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_PAYLOAD;
                return null;
            case MAVLINK_PARSE_STATE_GOT_PAYLOAD:
                this.m.generateCRC(this.m.payload.size());
                if (i2 != this.m.crc.getLSB()) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_IDLE;
                    this.stats.crcError();
                    return null;
                }
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_CRC1;
                return null;
            case MAVLINK_PARSE_STATE_GOT_CRC1:
                if (i2 != this.m.crc.getMSB()) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_IDLE;
                    this.stats.crcError();
                    return null;
                }
                this.stats.newPacket(this.m);
                if (!this.isMavlink2 || this.m.incompatFlags != 1) {
                    this.state = MAV_states.MAVLINK_PARSE_STATE_IDLE;
                    return this.m;
                }
                this.m.signature = new Signature();
                this.state = MAV_states.MAVLINK_PARSE_STATE_GOT_CRC2;
                return null;
            case MAVLINK_PARSE_STATE_GOT_CRC2:
                this.m.signature.signature.put((byte) i2);
                if (this.m.signature.signature.position() != 13) {
                    return null;
                }
                this.state = MAV_states.MAVLINK_PARSE_STATE_IDLE;
                Timber.v("Got a signed message");
                return this.m;
            default:
                return null;
        }
    }
}
