package com.MAVLink;

import java.nio.ByteBuffer;

public class Signature {
    public static final int MAX_SIGNATURE_SIZE = 13;
    public final ByteBuffer signature = ByteBuffer.allocate(13);
}
